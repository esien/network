/**
 * Created by esenaero on 12/07/16.
 */
(function($){

    $('#picSettings').on('click', function() {
        $("#settingsModal").modal('toggle');
    });


    $('#picUpload').on('click', function() {
        $("#uploadPictureModal").modal('toggle');
    });

    $('#createAlbum').on('click', function() {
        $("#createAlbumModal").modal('toggle');
    });

    $('#addContact').on('click', function() {
        $("#addContactModal").modal('toggle');
    });

    $('#changeUserSettings').on('click', function(){
        $("#userSettingsModal").modal('toggle');
    });


    
    $(".fancybox-thumb").fancybox({
        prevEffect	: 'none',
        nextEffect	: 'none',
        helpers	: {
            title	: {
                type: 'outside'
            },
            thumbs	: {
                width	: 50,
                height	: 50
            }
        }
    });

    $(".fancybox-effects-a").fancybox({
        helpers: {
            title : {
                type : 'outside'
            },
            overlay : {
                speedOut : 0
            }
        }
    });

    // init Masonry
    var $grid = $('.grid').masonry({
        itemSelector: '.grid-item'
    });
// layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });
    
})(jQuery);