package novruzov.esien.network.usermanagement.account.logic;

import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author Esien Novruzov
 */
public class PBKDF2Hasher implements Hasher {

    private static final String ITERATIONS = "private.network.account.iterations";
    private static final String KEY_LENGTH = "private.network.account.key.length";

    @Override
    public String hashPasswordAndSalt(String password, byte[] salt) {
        char[] passwordChars = password.toCharArray();
        int iterations = Integer.parseInt(ConfigurationManager.getValue(ITERATIONS));
        int hashLength = Integer.parseInt(ConfigurationManager.getValue(KEY_LENGTH));
        PBEKeySpec spec = new PBEKeySpec(passwordChars, salt, iterations, hashLength);
        byte[] hashedPassword = new byte[0];
        try {
            SecretKeyFactory key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hashedPassword = key.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return String.format("%x", new BigInteger(hashedPassword));
    }

}
