package novruzov.esien.network.usermanagement.account.logic;

/**
 * //TODO add java doc
 */
public interface Hasher {

    /**
     * //TODO add javadoc description
     * @param password
     * @param salt
     * @return
     */
    String hashPasswordAndSalt(String password, byte[] salt);
}
