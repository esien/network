package novruzov.esien.network.usermanagement.account.logic;


import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import java.security.SecureRandom;
import java.util.Random;

public class DefaultSaltGenerator implements SaltGenerator {

    @Override
    public byte[] generateSalt() {
        Random random = new SecureRandom();
        int saltLength = Integer.parseInt(ConfigurationManager.getValue("private.network.account.byte.length"));
        byte[] salt = new byte[saltLength];
        random.nextBytes(salt);
        return salt;
    }
}
