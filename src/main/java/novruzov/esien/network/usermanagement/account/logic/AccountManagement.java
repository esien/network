package novruzov.esien.network.usermanagement.account.logic;

import novruzov.esien.network.usermanagement.account.data.Account;

public interface AccountManagement {

    boolean createAccount(String username, String password);

    boolean changePassword(String username, String oldPassword, String newPassword);

    boolean changeUsername(String oldUsername, String newUsername);

    boolean deleteAccount(String username);

    boolean updateAccount(Account account);

}
