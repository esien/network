package novruzov.esien.network.usermanagement.account.logic;

import novruzov.esien.network.usermanagement.account.data.Account;
import novruzov.esien.network.usermanagement.account.repository.AccountRepository;
import novruzov.esien.network.usermanagement.data.UserTO;

import javax.inject.Inject;

/**
 * @author Esien Novruzov
 */
public class JpaLoginManagement implements LoginManagement {
    @Inject
    private AccountRepository accountRepository;

    @Inject
    private Hasher hasher;

    @Inject
    private SaltGenerator saltGenerator;

    @Override
    public boolean isValidLogin(String username, String password) {
        Account account = accountRepository.read(username);
        if (account == null){
            return false;
        }

        String accountHash = account.getHash();
        byte[] salt = account.getSalt();
        return accountHash.equals(hasher.hashPasswordAndSalt(password, salt));
    }

    @Override
    public boolean signUp(UserTO userTO) {
        if (userTO == null){
            return false;
        }
        String username = userTO.getUsername();
        if (isUsernameAvailable(username)) {
            byte[] salt = saltGenerator.generateSalt();
            String hashedPassword = hasher.hashPasswordAndSalt(userTO.getPassword(), salt);
            Account account = new Account(username, salt, hashedPassword);
            accountRepository.create(account);
            return true;
        } else
            return false;
    }

    private boolean isUsernameAvailable(String username){
        Account account = accountRepository.read(username);
        return account == null ? true : false;
    }
}
