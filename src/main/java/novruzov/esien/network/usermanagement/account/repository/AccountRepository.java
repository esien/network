package novruzov.esien.network.usermanagement.account.repository;

import novruzov.esien.network.usermanagement.account.data.Account;

/**
 * @author Esien Novruzov
 */
public interface AccountRepository {
    Account create(Account account);

    Account read(String username);

    Account update(Account account);

    void delete(Account account);
}
