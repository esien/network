package novruzov.esien.network.usermanagement.account.logic;

import novruzov.esien.network.usermanagement.data.UserTO;

import javax.transaction.Transactional;

@Transactional
public interface LoginManagement {

    boolean isValidLogin(String username, String password);

    @Transactional
    boolean signUp(UserTO userTO);
}
