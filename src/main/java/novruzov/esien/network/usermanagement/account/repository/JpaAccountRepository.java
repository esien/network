package novruzov.esien.network.usermanagement.account.repository;

import novruzov.esien.network.usermanagement.account.data.Account;

import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * @author Esien Novruzov
 */
@Default
@Transactional(value = Transactional.TxType.MANDATORY)
public class JpaAccountRepository implements AccountRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Account create(Account account) {
        em.persist(account);
        return account;
    }

    @Override
    public Account read(String username) {
        Account account = em.find(Account.class, username);
        return account;
    }

    @Override
    public Account update(Account account) {
        return em.merge(account);
    }

    @Override
    public void delete(Account account) {
        em.remove(account);
    }
}
