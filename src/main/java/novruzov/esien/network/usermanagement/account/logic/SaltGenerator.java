package novruzov.esien.network.usermanagement.account.logic;

/**
 * SaltGenerator class with a single method to generate a new random salt
 */
public interface SaltGenerator {

    /**
     * //TODO add java doc
     * @return
     */
    byte[] generateSalt();
}
