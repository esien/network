package novruzov.esien.network.usermanagement.account.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account {

    @Id
    @Column(name = "id")
    private String username;

    private byte[] salt;

    private String hash;

    public Account(String username, byte[] salt, String hash) {
        this.username = username;
        this.salt = salt;
        this.hash = hash;
    }

    public Account(byte[] salt, String hash) {
        this.salt = salt;
        this.hash = hash;
    }

    public Account() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
