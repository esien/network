package novruzov.esien.network.usermanagement.account.logic;

import novruzov.esien.network.usermanagement.account.data.Account;
import novruzov.esien.network.usermanagement.account.repository.AccountRepository;

import javax.inject.Inject;

/**
 * @author Esien Novruzov
 */
public class JpaAccountManagement implements AccountManagement {

    @Inject
    private Hasher hasher;

    @Inject
    private SaltGenerator saltGenerator;

    @Inject
    private AccountRepository accountRepository;

    @Override
    public boolean createAccount(String username, String password) {
        if (isAvailableAccount(username)){
            Account account = createAccountEntity(username, password);
            accountRepository.create(account);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean changePassword(String username, String oldPassword, String newPassword) {
       if (isCorrectOldPassword(username, oldPassword)){
           Account account = createAccountEntity(username, newPassword);
           accountRepository.update(account);
           return true;
       } else {
           return false;
       }
    }

    @Override
    public boolean deleteAccount(String username) {
        if (username == null){
            throw new IllegalArgumentException("username must be not null");
        }
        Account account = accountRepository.read(username);
        if (account == null)
            return false;
        accountRepository.delete(account);
        return true;
    }

    @Override
    public boolean changeUsername(String oldUsername, String newUsername) {
        if (oldUsername == null || newUsername == null){
            throw new IllegalArgumentException("username must be not null");
        }

        Account account = accountRepository.read(oldUsername);
        Account newAccount = new Account();
        newAccount.setHash(account.getHash());
        newAccount.setSalt(account.getSalt());
        newAccount.setUsername(newUsername);

        accountRepository.delete(account);
        accountRepository.create(newAccount);
        return true;
    }

    @Override
    public boolean updateAccount(Account account) {
        if (account == null) {
            return false;
        }
        accountRepository.update(account);
        return true;
    }

    private Account createAccountEntity(String username, String password){
        byte[] salt = saltGenerator.generateSalt();
        String hashedPassword = hasher.hashPasswordAndSalt(password, salt);
        Account account = new Account(username, salt, hashedPassword);
        return account;
    }

    private boolean isAvailableAccount(String username){
        return accountRepository.read(username) == null;
    }

    private boolean isCorrectOldPassword(String username, String oldPassword){
        Account account = accountRepository.read(username);
        if (account == null){
            return false;
        }
        byte[] salt = account.getSalt();
        String accountHash = account.getHash();
        if (accountHash.equals(hasher.hashPasswordAndSalt(oldPassword, salt))){
            return true;
        } else {
            return false;
        }

    }

}
