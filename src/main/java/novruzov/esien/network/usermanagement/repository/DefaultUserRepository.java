package novruzov.esien.network.usermanagement.repository;

import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.generic.repository.JpaRepository;

import javax.enterprise.inject.Default;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Esien Novruzov
 */
@Default
@Transactional
public class DefaultUserRepository extends JpaRepository<User> implements UserRepository {

    @Override
    public User findByUsername(String username) {
        String query = "from User as user where user.username = :username";
        List<User> userList = entityManager.createQuery(query, User.class).setParameter("username", username).getResultList();
        if (userList != null && !userList.isEmpty()){
            return userList.get(0);
        } else
            return null;
    }

    @Override
    public User findByEmail(String email) {
        String query = "from User as user where user.email = :email";
        User user = entityManager.createQuery(query, User.class).setParameter("email", email).getSingleResult();
        return user;
    }

    @Override
    public boolean updateFirstName(String username, String firstName) {
        if (username == null || firstName == null){
            return false;
        }
        User user = findByUsername(username);
        if (user == null){
            return false;
        }
        user.setFirstName(firstName);
        update(user);
        return true;
    }

    @Override
    public boolean updateLastName(String username, String lastName) {
        if (username == null || lastName == null){
            return false;
        }
        User user = findByUsername(username);
        if (user == null){
            return false;
        }
        user.setLastName(lastName);
        update(user);
        return true;
    }
}
