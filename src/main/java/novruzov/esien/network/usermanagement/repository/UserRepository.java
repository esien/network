package novruzov.esien.network.usermanagement.repository;

import novruzov.esien.network.usermanagement.generic.repository.GenericEntityRepository;
import novruzov.esien.network.usermanagement.data.User;

/**
 * @author Esien Novruzov
 */
public interface UserRepository extends GenericEntityRepository<User> {
    User findByUsername(String username);

    User findByEmail(String email);

    boolean updateFirstName(String username, String firstName);

    boolean updateLastName(String username, String lastName);


}
