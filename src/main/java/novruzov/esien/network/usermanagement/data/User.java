package novruzov.esien.network.usermanagement.data;


import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.usermanagement.generic.data.GenericEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Esien Novruzov
 */
@Entity
@Table(name = "Users")
public class User extends GenericEntity<User> {
    @PrimaryKeyJoinColumn
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    @Temporal(TemporalType.DATE)
    private Date lastLogged;
    private String aboutMe;
    @Temporal(TemporalType.DATE)
    private Date memberSince;
    private String profilePictureUrl;
    @OneToMany(mappedBy = "owner",fetch = FetchType.LAZY)
    private Set<Picture> ownedPictureEntities = new HashSet<>();
    //TODO add profile pic, pictures, albums, tracks, contacts


    public User(String username, String email, String firstName, String lastName, Date lastLogged, String aboutMe, Date memberSince, String profilePictureUrl, Set<Picture> ownedPictureEntities) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogged = lastLogged;
        this.aboutMe = aboutMe;
        this.memberSince = memberSince;
        this.profilePictureUrl = profilePictureUrl;
        this.ownedPictureEntities = ownedPictureEntities;
    }

    public User(String username, String email, String firstName, String lastName, Date lastLogged, String aboutMe, Date memberSince, Set<Picture> ownedPictureEntities) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogged = lastLogged;
        this.aboutMe = aboutMe;
        this.memberSince = memberSince;
        this.ownedPictureEntities = ownedPictureEntities;
    }

    public User(String username, String email, String firstName, String lastName, Date lastLogged, String aboutMe, Date memberSince) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogged = lastLogged;
        this.aboutMe = aboutMe;
        this.memberSince = memberSince;
    }

    public User(String username, String email, Date lastLogged, String aboutMe, Date memberSince) {
        this.username = username;
        this.email = email;
        this.lastLogged = lastLogged;
        this.aboutMe = aboutMe;
        this.memberSince = memberSince;
    }

    public User(String username, String email, Date lastLogged) {
        this.username = username;
        this.email = email;
        this.lastLogged = lastLogged;
    }

    public User(UserTO userTO){
        this.username = userTO.getUsername();
        this.email = userTO.getEmail();
        this.memberSince = userTO.getRegistrationDate();
        if (userTO.getFirstName() != null)
            this.firstName = userTO.getFirstName();
        if (userTO.getLastName() != null)
            this.lastName = userTO.getLastName();
    }

    public User() {
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profileBildUrl) {
        this.profilePictureUrl = profileBildUrl;
    }

    public Set<Picture> getOwnedPictureEntities() {
        return ownedPictureEntities;
    }

    public void setOwnedPictureEntities(Set<Picture> ownedPictureEntities) {
        this.ownedPictureEntities = ownedPictureEntities;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Date getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(Date memberSince) {
        this.memberSince = memberSince;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getLastLogged() {
        return lastLogged;
    }

    public void setLastLogged(Date lastLogged) {
        this.lastLogged = lastLogged;
    }

}
