package novruzov.esien.network.usermanagement.filter;

/**
 * @author Esien Novruzov
 */
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.presentation.Login;
import novruzov.esien.network.usermanagement.service.UserManagementService;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Esien Novruzov on 17.01.16.
 */
@WebFilter(displayName = "viewFilter", urlPatterns = "/uploads/*")
public class PictureViewFilter implements Filter {

    @Inject
    private Login loginBean;

    @Inject
    private UserManagementService userManagementService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        String[] splitedParts = uri.split("/");

        String requestedResourceName = splitedParts[splitedParts.length - 1];

        User currentUser = userManagementService.findUserByUsername(loginBean.getUsername());

        if (currentUser == null){
            res.sendRedirect("/login");
        } else {
            chain.doFilter(request, response);
        }


    }

    @Override
    public void destroy() {

    }

}