package novruzov.esien.network.usermanagement.filter;

import novruzov.esien.network.usermanagement.presentation.Login;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Esien Novruzov
 */
@WebFilter(filterName = "AuthFilter", urlPatterns = {"/*"})
public class AuthenticationFilter implements Filter {

    @Inject
    private ServletContext servletContext;

    @Inject
    private Login loginBean;

    private static final Logger LOGGER = Logger.getLogger(AuthenticationFilter.class.getName());

    public AuthenticationFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String requestURI = request.getRequestURI();

        /*
        if (requestURI.endsWith(".jsf") && !requestURI.contains("javax.faces.resource")) {
            response.sendRedirect("../error");
            return;
        }
        */
        /*

        // Prevent rendering of JSESSIONID in URLs for all outgoing links
        HttpServletResponseWrapper wrappedResponse = new HttpServletResponseWrapper(
                response) {
            @Override
            public String encodeRedirectUrl(String url) {
                return url;
            }

            @Override
            public String encodeRedirectURL(String url) {
                return url;
            }

            @Override
            public String encodeUrl(String url) {
                return url;
            }

            @Override
            public String encodeURL(String url) {
                return url;
            }
        };

        if (requestURI.contains("jsessionid")) {
            filterChain.doFilter(request, wrappedResponse);
            return;
        }
        */

        if (isPublicRequestOrRegisteredUser(requestURI, session)) {
            if (session != null && session.getAttribute("username") != null) {
                session.setAttribute("username", loginBean.getUsername());
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            LOGGER.setLevel(Level.WARNING);
            LOGGER.warning("Unauthorized request.");
            response.sendRedirect(servletContext.getContextPath());
        }
    }

    @Override
    public void destroy() {

    }

    private boolean isPublicRequestOrRegisteredUser(String requestURI, HttpSession session) {
        return
                requestURI.contains("login")
                        //|| requestURI.contains("jpg")
                        || requestURI.contains("background.js")
                        || requestURI.contains("main.js")
                        || requestURI.contains("/gallery/")
                        || requestURI.endsWith("network/")
                        || requestURI.contains("javax.faces.resource")
                        || requestURI.contains("Bootstrap")
                        || requestURI.contains("registration")
                        || requestURI.contains("images")
                        || requestURI.endsWith("css")
                        || (session != null && session.getAttribute("username") != null);
    }
}
