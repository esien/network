package novruzov.esien.network.usermanagement.utility;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * @author Esien Novruzov
 */
public final class SessionService {

    public static HttpSession getCurrentSession(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        return session;
    }

}
