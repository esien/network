package novruzov.esien.network.usermanagement.utility;

import java.util.ResourceBundle;

public final class ConfigurationManager {

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("configuration");

    private ConfigurationManager(){}


    public static String getValue(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}
