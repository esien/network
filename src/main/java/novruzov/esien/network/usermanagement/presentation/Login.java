package novruzov.esien.network.usermanagement.presentation;

import novruzov.esien.network.usermanagement.service.UserManagementService;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Esien Novruzov
 */
@Named
@SessionScoped
public class Login implements Serializable {
    @Size(min=4, max=20)
    @NotNull
    private String username;
    @NotNull
    private String password;
    private static final String JSF_REDIRECT = "?faces-redirect=true";
    transient private String invalidUsernameErrorMessage = ConfigurationManager.getValue("private.network.messages.invalid.username");
    private String errorMessage;

    @Inject
    transient private  UserManagementService userManagementService;

    @Inject
    transient private ServletContext servletContext;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getInvalidUsernameErrorMessage() {
        return invalidUsernameErrorMessage;
    }

    public void setInvalidUsernameErrorMessage(String invalidUsernameErrorMessage) {
        this.invalidUsernameErrorMessage = invalidUsernameErrorMessage;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String processRequst(){
        if (userManagementService.isValidLogin(username, password)){
            userManagementService.setLastLogin();
            password = null; // we do not need to save password in a session
            return "jsf/main" + JSF_REDIRECT;
        } else {
            userManagementService.logOut();
            errorMessage = ConfigurationManager.getValue("private.network.messages.invalid.login");
            return null;
        }
    }

    public String logout(){
        userManagementService.logOut();
        return "main" + JSF_REDIRECT;
    }
}
