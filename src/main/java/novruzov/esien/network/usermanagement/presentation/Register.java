package novruzov.esien.network.usermanagement.presentation;

import novruzov.esien.network.usermanagement.data.UserTO;
import novruzov.esien.network.usermanagement.service.UserManagementService;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Esien Novruzov
 */
@RequestScoped
@Named
public class Register {
    public static final String ERROR_MESSAGE_KEY = "private.network.messages.invalid.registration";
    @Inject
    private UserManagementService userManagementService;

    private static final String JSF_REDIRECT = "?faces-redirect=true";

    @NotNull
    private String email; //TODO add email validation
    private String firstName;
    private String lastName;
    @NotNull
    @Size(min = 4, max = 20)
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String retypedPassword;
    private String retypedPasswordErrorMessage;
    private String errorMessage;

    public Register(){
    }

    public String registerUser(){
        if (!password.equals(retypedPassword)){
            retypedPasswordErrorMessage = ConfigurationManager.getValue("private.network.messages.invalid.password.retyped");
            return null;
        }
        UserTO userToBeCreated;
        //TODO bean validation - all properties using regex
        if (firstName != null && lastName !=null){
            userToBeCreated = new UserTO(email, firstName, lastName, username, password, new Date());
        } else {
            userToBeCreated = new UserTO(email, username, password, new Date());
        }
        if (userManagementService.registerNewUser(userToBeCreated))
            return "index" + JSF_REDIRECT;
        else {
            errorMessage = ConfigurationManager.getValue(ERROR_MESSAGE_KEY);
            return null;
        }
    }

    public String getRetypedPasswordErrorMessage() {
        return retypedPasswordErrorMessage;
    }

    public void setRetypedPasswordErrorMessage(String retypedPasswordErrorMessage) {
        this.retypedPasswordErrorMessage = retypedPasswordErrorMessage;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
