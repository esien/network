package novruzov.esien.network.usermanagement.presentation;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.mediamanagement.service.PictureService;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.service.UserManagementService;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Esien Novruzov
 */
@Named
@RequestScoped
public class Home {

    private String firstNameOrUsername;

    @Inject
    private Login login;

    @Inject
    private UserManagementService userManagementService;

    @Inject
    private PictureService pictureService;

    private List<Picture> allUserPictures;

    public String getFirstNameOrUsername() {
        User user = userManagementService.findUserByUsername(login.getUsername());
        if (user.getFirstName() != null && !user.getFirstName().isEmpty())
            return user.getFirstName();
        return firstNameOrUsername;
    }

    public List<Picture> getAllUserPictures(){
        User owner = userManagementService.findUserByUsername(login.getUsername());
        if (owner == null){
            throw new NullPointerException("the user is not found");
        }

        List<Picture> pictures = pictureService.getAllPicturesByUsername(login.getUsername());
        initPictureURLs(pictures);
        return pictures;
    }


    public void setAllUserPictures(List<Picture> allUserPictures) {
        this.allUserPictures = allUserPictures;
    }

    public void setFirstNameOrUsername(String firstNameOrUsername) {
        this.firstNameOrUsername = firstNameOrUsername;
    }


    private void initPictureURLs(List<Picture> pictures){
        for (Picture pic: pictures){
            String pictureUrl = ConfigurationManager.getValue("private.network.uploaded.relative.path") + File.separator + pic.getResourceName();
            pic.setResourceName(pictureUrl);
        }
    }


}
