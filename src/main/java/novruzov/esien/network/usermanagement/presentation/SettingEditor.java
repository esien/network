package novruzov.esien.network.usermanagement.presentation;

import novruzov.esien.network.mediamanagement.service.PictureService;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.service.UserManagementService;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import java.io.File;

/**
 * @author Esien Novruzov
 */
@Named
@RequestScoped
public class SettingEditor {

    private String profilePictureUrl;
    private String username;
    private String password;
    private String retypedPassword;
    private String firstName;
    private String lastName;
    private String aboutMe;
    private String changeStatus;
    private String errorMessageForPassword;

    private Part profilePicture;

    //Placeholders
    private String firstNamePlaceHolder;
    private String lastNamePlaceHolder;
    private String usernamePlaceHolder;
    private String aboutmePlaceHolder;

    @Inject
    private UserManagementService userManagementService;

    @Inject
    private PictureService pictureService;

    @Inject
    private Login loginBean;

    @Inject
    private ServletContext servletContext;

    private static final String STATUS_KEY_SUCCESS = "private.network.messages.settings.success";
    private static final String STATUS_KEY_FAILED = "private.network.messages.settings.failed";
    private static final String PASSWORD_REQUIRED_KEY = "private.network.messages.password.required";
    private static final String STATUS_KEY_USERNAME_NOT_AVAILABLE = "private.network.messages.settings.change.username.failed";


    public String getProfilePictureUrl() {
        String picUrl = userManagementService.findUserByUsername(loginBean.getUsername()).getProfilePictureUrl();
        if (picUrl == null){
            return servletContext.getContextPath() + File.separator + "images" + File.separator + ConfigurationManager.getValue("private.network.default.profile.picture");
        }
        String finalUrl = ConfigurationManager.getValue("private.network.uploaded.relative.path") + picUrl;

        return finalUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public Part getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Part profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getAboutmePlaceHolder() {
        User user = userManagementService.findUserByUsername(loginBean.getUsername());
        if (user == null)
            return "";

        return user.getAboutMe();
    }

    public void setAboutmePlaceHolder(String aboutmePlaceHolder) {
        this.aboutmePlaceHolder = aboutmePlaceHolder;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getUsernamePlaceHolder() {
        usernamePlaceHolder = loginBean.getUsername();
        return usernamePlaceHolder;
    }

    public void setUsernamePlaceHolder(String usernamePlaceHolder) {
        this.usernamePlaceHolder = usernamePlaceHolder;
    }

    public String getLastNamePlaceHolder() {
        User user = userManagementService.findUserByUsername(loginBean.getUsername());
        if (user == null)
            return "";
        lastNamePlaceHolder = user.getLastName();
        return lastNamePlaceHolder;
    }

    public void setLastNamePlaceHolder(String lastNamePlaceHolder) {
        this.lastNamePlaceHolder = lastNamePlaceHolder;
    }

    public String getFirstNamePlaceHolder() {
        User user = userManagementService.findUserByUsername(loginBean.getUsername());
        if (user == null){
            return "";
        }
        firstNamePlaceHolder = user.getFirstName();
        return firstNamePlaceHolder;
    }

    public void setFirstNamePlaceHolder(String firstNamePlaceHolder) {
        this.firstNamePlaceHolder = firstNamePlaceHolder;
    }

    public String getErrorMessageForPassword() {
        return errorMessageForPassword;
    }

    public void setErrorMessageForPassword(String errorMessageForPassword) {
        this.errorMessageForPassword = errorMessageForPassword;
    }

    public String getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(String changeStatus) {
        this.changeStatus = changeStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void deleteProfilePicture(){
        pictureService.deletePicture(userManagementService.findUserByUsername(loginBean.getUsername()).getProfilePictureUrl());
    }

    public void processRequest() {

        if(profilePicture != null && profilePicture.getSize() != 0){
            pictureService.uploadProfilePicture(profilePicture);
        }

        if (firstName != null && !firstName.trim().isEmpty()) {
            userManagementService.setUserFirstName(loginBean.getUsername(), firstName);
            firstName = null;
        }

        if (aboutMe != null && !aboutMe.isEmpty()){
            userManagementService.setAboutMe(loginBean.getUsername(), aboutMe);
            aboutMe = null;
        }

        if (lastName != null && !lastName.trim().isEmpty()) {
            userManagementService.setUserLastName(loginBean.getUsername(), lastName);
            lastName = null;
        }

        if (username != null && !username.trim().isEmpty()) {
            if (password != null && !password.isEmpty()) {

                if (userManagementService.isValidLogin(loginBean.getUsername(), password)) {
                    if (userManagementService.changeUsername(username)){
                        setChangeStatus(ConfigurationManager.getValue(STATUS_KEY_SUCCESS));
                    } else {
                        setChangeStatus(ConfigurationManager.getValue(STATUS_KEY_USERNAME_NOT_AVAILABLE));
                    }
                    username = null;
                } else {
                    setChangeStatus(ConfigurationManager.getValue(STATUS_KEY_FAILED));
                    username = null;
                    password = null;
                    return;
                }
            } else {
                setErrorMessageForPassword(ConfigurationManager.getValue(PASSWORD_REQUIRED_KEY));
                username = null;
                return;
            }
        }
    }

}
