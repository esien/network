package novruzov.esien.network.usermanagement.service;

import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.data.UserTO;

import javax.transaction.Transactional;

/**
 * @author Esien Novruzov
 */
@Transactional
public interface UserManagementService {

    boolean isValidLogin(String username, String password);

    boolean registerNewUser(UserTO userTO);

    boolean updateInfo(UserTO userTo);

    boolean changePassword(String oldPassword, String newPassword, String retypedPassword);

    boolean changeUsername(String newUsername);

    boolean setUserFirstName(String username, String firstName);

    boolean setUserLastName(String username, String lastName);

    void setAboutMe(String username, String aboutMe);

    boolean deleteAccount();

    void setLastLogin();

    User findUserByUsername(String username);

    void logOut();
}
