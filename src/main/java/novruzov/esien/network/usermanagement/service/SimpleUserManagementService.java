package novruzov.esien.network.usermanagement.service;

import novruzov.esien.network.mediamanagement.service.PictureService;
import novruzov.esien.network.usermanagement.account.logic.AccountManagement;
import novruzov.esien.network.usermanagement.account.logic.LoginManagement;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.data.UserTO;
import novruzov.esien.network.usermanagement.presentation.Login;
import novruzov.esien.network.usermanagement.repository.UserRepository;
import novruzov.esien.network.usermanagement.utility.SessionService;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.transaction.Transactional;
import java.util.Date;

/**
 * @author Esien Novruzov
 */
@Default
@Transactional
public class SimpleUserManagementService implements UserManagementService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private LoginManagement loginManagement;

    @Inject
    private AccountManagement accountManagement;


    @Inject
    private Login loginBean;

    @Transactional
    @Override
    public boolean isValidLogin(String username, String password) {
        if (loginManagement.isValidLogin(username, password)){
            HttpSession session = SessionService.getCurrentSession();
            session.setAttribute("username", username);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @Override
    public boolean registerNewUser(UserTO userTO) {
        if (loginManagement.signUp(userTO)){
            userRepository.create(new User(userTO));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean changePassword(String oldPassword, String newPassword, String retypedPassword) {
        if (!newPassword.equals(retypedPassword)){
            throw new IllegalArgumentException("Passwords are not equal!");
        }
        return accountManagement.changePassword(loginBean.getUsername(), oldPassword, newPassword);
    }

    @Override
    public boolean deleteAccount() {
        String username = loginBean.getUsername();
        if (username == null || username.isEmpty())
            return false;
        return accountManagement.deleteAccount(username);
    }

    @Override
    public boolean updateInfo(UserTO userTo) {
        if (userTo == null){
            return false;
        }
        userRepository.update(new User(userTo));
        return true;
    }

    @Override
    public void logOut() {
        HttpSession session = SessionService.getCurrentSession();
        session.invalidate();
    }

    @Override
    public void setLastLogin() {
        String username = loginBean.getUsername();
        User user = userRepository.findByUsername(username);
        user.setLastLogged(new Date());
        userRepository.update(user);
    }

    @Override
    public boolean changeUsername(String newUsername) {
        if (isUsernameAvailable(newUsername)){
            return setUsername(newUsername);
        }
        return false;
    }

    @Override
    public boolean setUserFirstName(String username, String firstName) {
        return userRepository.updateFirstName(username, firstName);
    }

    @Override
    public boolean setUserLastName(String username, String lastName) {
        return userRepository.updateLastName(username, lastName);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    private boolean isUsernameAvailable(String username){
        User foundedUser = userRepository.findByUsername(username);
        return foundedUser == null? true : false;
    }

    @Override
    public void setAboutMe(String username, String aboutMe) {
        User foundedUser = userRepository.findByUsername(username);
        foundedUser.setAboutMe(aboutMe);
        userRepository.update(foundedUser);
    }

    private boolean setUsername(String newUsername){
        String oldUsername = loginBean.getUsername();

        User user = userRepository.findByUsername(oldUsername);
        if (user == null || newUsername == null){
            return false;
        }

        user.setUsername(newUsername);

        accountManagement.changeUsername(oldUsername, user.getUsername());
        userRepository.update(user);
        loginBean.setUsername(newUsername);
        return true; //TODO nonsense

    }
}
