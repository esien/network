package novruzov.esien.network.usermanagement.generic.repository;

import novruzov.esien.network.usermanagement.generic.data.GenericEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author Martin Schaffoener
 */
@Transactional(value = Transactional.TxType.MANDATORY)
public abstract class JpaRepository <EntityType extends GenericEntity> implements GenericEntityRepository<EntityType> {

    protected final Class<EntityType> entityTypeClass;

    protected JpaRepository() {

        Class<?> cls = getClass();
        while (!(cls.getSuperclass() == null || cls.getSuperclass().equals(JpaRepository.class))) {
            cls = cls.getSuperclass();
        }

        if (cls.getSuperclass() == null) {
            throw new RuntimeException("Unexpected exception occurred.");
        }

        this.entityTypeClass = ((Class) ((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    protected EntityManager entityManager;

    @Override
    public EntityType create(EntityType entity) {
        entityManager.persist(entity);
        entityManager.flush();
        return entity;
    }

    @Override
    public EntityType read(Integer id) {
        return entityManager.find(entityTypeClass, id);
    }

    @Override
    public void update(EntityType entity) {
        entityManager.merge(entity);
    }

    @Override
    public void delete(EntityType entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<EntityType> readAll() {
        return entityManager.createQuery("from " + entityTypeClass.getName(), entityTypeClass).getResultList();
    }

}