package novruzov.esien.network.usermanagement.generic.repository;

import novruzov.esien.network.usermanagement.generic.data.GenericEntity;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Esien Novruzov
 */
@Transactional
public interface  GenericEntityRepository<EntityType extends GenericEntity> {

    EntityType create(EntityType entityType);

    EntityType read(Integer id);

    List<EntityType> readAll();

    void update(EntityType entityType);

    void delete(EntityType entityType);
}