package novruzov.esien.network.mediamanagement.util;

/**
 * @author Esien Novruzov
 */
public interface FileNameGenerator {

    String generateNewFileName();

    String generateNewFileName(int fileNameLength);
}
