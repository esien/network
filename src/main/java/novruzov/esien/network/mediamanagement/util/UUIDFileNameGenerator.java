package novruzov.esien.network.mediamanagement.util;

import javax.enterprise.inject.Default;
import java.util.UUID;

/**
 * @author Esien Novruzov
 */
@Default
public class UUIDFileNameGenerator implements FileNameGenerator {

    @Override
    public String generateNewFileName() {
        String randomFileName = UUID.randomUUID().toString();
        randomFileName = randomFileName.replaceAll("-","");
        return randomFileName;
    }

    @Override
    public String generateNewFileName(int fileNameLength) {
        if (fileNameLength <= 0 || fileNameLength > 32){
            return generateNewFileName();
        }
        return generateNewFileName().substring(0 , fileNameLength);
    }
}
