package novruzov.esien.network.mediamanagement.data;

import novruzov.esien.network.mediamanagement.data.transfer.PictureTO;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.generic.data.GenericEntity;
import novruzov.esien.network.usermanagement.repository.UserRepository;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author Esien Novruzov
 */
@Entity
public class Picture extends GenericEntity<Picture> {

    private String title;

    private String description;

    private String resourceName;

    @Temporal(TemporalType.DATE)
    private Date publicationDate;

    @ManyToOne
    @JoinColumn(name = "OwnerID")
    private User owner;

    public Picture() {
        this.publicationDate = new Date();
    }

    public Picture(String title, String description, String resourceName, Date publicationDate, User owner) {
        this.title = title;
        this.description = description;
        this.resourceName = resourceName;
        this.publicationDate = publicationDate;
        this.owner = owner;
    }

    public Picture(String title, String description, String resourceName, Date publicationDate) {
        this.title = title;
        this.description = description;
        this.resourceName = resourceName;
        this.publicationDate = publicationDate;
    }

    public Picture(PictureTO pictureTO) {
        this.title = pictureTO.getTitle();
        this.description = pictureTO.getDescription();
        this.publicationDate = pictureTO.getPublicationDate();
        this.resourceName = pictureTO.getResourceName();
        this.owner = pictureTO.getOwner();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", resourceName='" + resourceName + '\'' +
                ", publicationDate=" + publicationDate +
                ", owner=" + owner +
                '}';
    }
}
