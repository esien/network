package novruzov.esien.network.mediamanagement.data.transfer;

import novruzov.esien.network.usermanagement.data.User;

import java.util.Date;

/**
 * @author Esien Novruzov
 */
public class PictureTO {
    private String title;
    private String description;
    private String resourceName;
    private Date publicationDate;
    private User owner;

    public PictureTO() {
    }

    public PictureTO(String title, String description, String resourceName, Date publicationDate, User owner) {
        this.title = title;
        this.description = description;
        this.resourceName = resourceName;
        this.publicationDate = publicationDate;
        this.owner = owner;
    }

    public PictureTO(String title, String description, User owner) {
        this.title = title;
        this.description = description;
        this.publicationDate = new Date();
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
