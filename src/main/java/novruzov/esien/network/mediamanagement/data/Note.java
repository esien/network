package novruzov.esien.network.mediamanagement.data;

import novruzov.esien.network.usermanagement.generic.data.GenericEntity;

import javax.persistence.Entity;
import java.util.Date;

/**
 * @author Esien Novruzov
 */
@Entity
public class Note extends GenericEntity<Note> {

    private String author;
    private Date publicationDate;
    private String content;

    public Note(String author, Date publicationDate, String content) {
        this.author = author;
        this.publicationDate = publicationDate;
        this.content = content;
    }

    public Note() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
