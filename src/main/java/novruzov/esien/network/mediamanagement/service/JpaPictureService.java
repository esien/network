package novruzov.esien.network.mediamanagement.service;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.mediamanagement.data.transfer.PictureTO;
import novruzov.esien.network.mediamanagement.repository.PictureBlobRepository;
import novruzov.esien.network.mediamanagement.repository.PictureMetaRepository;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.presentation.Login;
import novruzov.esien.network.usermanagement.repository.UserRepository;

import javax.inject.Inject;
import javax.servlet.http.Part;
import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Esien Novruzov
 */
@Transactional
public class JpaPictureService implements PictureService {

    @Inject
    private PictureMetaRepository pictureMetaRepository;

    @Inject
    private PictureBlobRepository pictureBlobRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private Login loginBean;

    @Override
    public List<String> getAllPicturesUrlsByUsername(String username) {

        List<Picture> pictures = pictureMetaRepository.readAllByUsername(username);
        List<String> picturesURLs = extractUrlsFromList(pictures);
        return picturesURLs;
    }

    @Override
    public List<Picture> getAllPicturesByUsername(String username) {
        return pictureMetaRepository.readAllByUsername(username);
    }

    @Override
    public Picture getPictureByPictureId(int picID) {
        //TODO implement
        return null;
    }

    @Override
    public boolean deletePicture(String fileName) {
        if (fileName == null) {
            return false;
        }
        Picture picture = pictureMetaRepository.read(loginBean.getUsername(), fileName);
        pictureMetaRepository.delete(picture);
        return pictureBlobRepository.deletePicture(fileName);
    }

    @Override
    public Picture uploadPicture(PictureTO pictureTO, Part part) {

        String resourceName = pictureBlobRepository.persistPicture(part);
        if (resourceName == null) {
            return null;
        } else {
            pictureTO.setResourceName(resourceName);
            User owner = userRepository.findByUsername(loginBean.getUsername());
            pictureTO.setOwner(owner);
            Picture picture = pictureMetaRepository.create(new Picture(pictureTO));

            return picture;
        }
    }

    @Override
    public String uploadProfilePicture(Part part) {
        String fileName = pictureBlobRepository.persistPicture(part, "profiles");
        String thumbName = fileName.split("\\.")[0] + ".thumb." + fileName.split("\\.")[1];
        userRepository.findByUsername(loginBean.getUsername()).setProfilePictureUrl(File.separator + "profiles" + File.separator + thumbName);

        return thumbName;
    }

    private List<String> extractUrlsFromList(List<Picture> pictures) {
        List<String> picturesURLs = new ArrayList<>();
        pictures.stream().forEach(picture -> picturesURLs.add(picture.getResourceName()));
        return picturesURLs;
    }
}
