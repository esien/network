package novruzov.esien.network.mediamanagement.service;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.mediamanagement.data.transfer.PictureTO;

import javax.servlet.http.Part;
import java.util.List;

/**
 * @author Esien Novruzov
 */
public interface PictureService {

    List<Picture> getAllPicturesByUsername(String username);

    List<String> getAllPicturesUrlsByUsername(String username);

    Picture getPictureByPictureId(int picID);

    Picture uploadPicture(PictureTO pictureTO, Part part);

    boolean deletePicture(String fileName);

    String uploadProfilePicture(Part part);
}
