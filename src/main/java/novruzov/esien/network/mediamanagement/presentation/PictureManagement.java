package novruzov.esien.network.mediamanagement.presentation;

import novruzov.esien.network.mediamanagement.service.PictureService;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.presentation.Login;
import novruzov.esien.network.usermanagement.service.UserManagementService;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * @author Esien Novruzov
 */
@Named
@RequestScoped
public class PictureManagement {

    @Inject
    private PictureService pictureService;

    @Inject
    private UserManagementService userManagementService;

    @Inject
    private Login login;

    private String filePathToDelete;

    public String getFilePathToDelete() {
        return filePathToDelete;
    }

    public void setFilePathToDelete(String filePathToDelete) {
        this.filePathToDelete = filePathToDelete;
    }

    public void deletePicture(){
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, String> requestParameterMap = ec.getRequestParameterMap();
        String hiddenValue = null;
        for (String key: requestParameterMap.keySet()){
            if (key.endsWith("deletePicForm:hidden")){
                hiddenValue = requestParameterMap.get(key);
            }
        }

        String filePath = hiddenValue.split("/")[hiddenValue.split("/").length - 1];
        setFilePathToDelete(filePath);
        pictureService.deletePicture(filePathToDelete);
    }
}
