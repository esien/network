package novruzov.esien.network.mediamanagement.presentation;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.mediamanagement.data.transfer.PictureTO;
import novruzov.esien.network.mediamanagement.service.PictureService;
import novruzov.esien.network.usermanagement.presentation.Login;
import novruzov.esien.network.usermanagement.service.UserManagementService;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * @author Esien Novruzov
 */
@Named
@RequestScoped
public class PictureUploader {

    private String title;

    private String description;

    private Part filePart;

    private Part profilePicPart;

    @Inject
    private Login loginBean;

    private static final String UPLOAD_ERROR_MESSAGE_KEY = "private.network.messages.upload.failed";
    private static final String UPLOAD_SUCCESS_MESSAGE_KEY = "private.network.messages.upload.success";

    private String uploadStatusMessage;

    @Inject
    private PictureService pictureService;

    @Inject
    private UserManagementService userService;

    public Part getProfilePicPart() {
        return profilePicPart;
    }

    public void setProfilePicPart(Part profilePicPart) {
        this.profilePicPart = profilePicPart;
    }

    public String getUploadStatusMessage() {
        return uploadStatusMessage;
    }

    public void setUploadStatusMessage(String uploadStatusMessage) {
        this.uploadStatusMessage = uploadStatusMessage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Part getFilePart() {
        return filePart;
    }

    public void setFilePart(Part filePart) {
        this.filePart = filePart;
    }

    public String upload() {
        PictureTO picture = new PictureTO(title, description, userService.findUserByUsername(loginBean.getUsername()));
        Picture uploadedPicture = pictureService.uploadPicture(picture, filePart);

        if (uploadedPicture == null) {
            setUploadStatusMessage(ConfigurationManager.getValue(UPLOAD_ERROR_MESSAGE_KEY));
            return null;
        } else {
            setUploadStatusMessage(ConfigurationManager.getValue(UPLOAD_SUCCESS_MESSAGE_KEY));
        }

        this.title = null;
        this.description = null;
        return "/you";
    }

    public String uploadProfilePicture() {
        pictureService.uploadProfilePicture(profilePicPart);
        return null;
    }
}
