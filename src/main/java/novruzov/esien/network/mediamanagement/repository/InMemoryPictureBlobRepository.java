package novruzov.esien.network.mediamanagement.repository;

import net.coobird.thumbnailator.Thumbnails;
import novruzov.esien.network.mediamanagement.util.FileNameGenerator;
import novruzov.esien.network.usermanagement.utility.ConfigurationManager;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Esien Novruzov
 */
public class InMemoryPictureBlobRepository implements PictureBlobRepository {

    @Inject
    private FileNameGenerator fileNameGenerator;

    @Inject
    private ServletContext servletContext;

    private static Logger LOGGER = Logger.getLogger(InMemoryPictureBlobRepository.class.getName());

    @Override
    public String persistPicture(Part part) {
        return persistPicture(part, null);
    }

    @Override
    public String persistPicture(Part part, String path) {
        String submittedFileExtension = "." + part.getSubmittedFileName().split("\\.")[1];

        if (!isValidFileExtension(submittedFileExtension)) {
            LOGGER.warning("Error while trying to upload a file with extension: " + submittedFileExtension);
            return null;
        }

        String generatedFileName = fileNameGenerator.generateNewFileName();
        String configPath = ConfigurationManager.getValue("private.network.upload.path");
        String uploadPath = (path == null ? configPath : configPath + File.separator + path);
        File profileDir = new File(uploadPath);
        if (profileDir.mkdirs()){
            LOGGER.info("profile subdirectory is created");
        }

        String pathname = uploadPath + File.separator + generatedFileName + submittedFileExtension;
        File file = new File(pathname);

        try (
                OutputStream out = new FileOutputStream(file);
                InputStream fileContent = part.getInputStream()) {

            int read;
            final byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            Thumbnails.of(file)
                    .size(640, 480)
                    .useOriginalFormat()
                    .keepAspectRatio(true)
                    .outputQuality(1)
                    .toFile(new File(uploadPath + File.separator + generatedFileName + ".thumb" + submittedFileExtension));


        } catch (IOException e) {
            LOGGER.setLevel(Level.WARNING);
            e.printStackTrace();
            LOGGER.warning("IO Exception while trying to upload a file to " + uploadPath + File.separator + generatedFileName);
            return null;
        }

        return generatedFileName + submittedFileExtension;
    }

    @Override
    public boolean deletePicture(String resourceName) {
        String path = ConfigurationManager.getValue("private.network.upload.path") + resourceName;
        if (path.contains(".thumb.")) {
            String originalFileName = path.split("\\.")[0] + "." + path.split("\\.")[2];
            File file = new File(originalFileName);
            file.delete();
        }
        String thumbPath = path.split("\\.")[0] + ".thumb." + path.split("\\.")[1];
        if (!path.contains(".thumb.") && !deleteThumbnail(thumbPath)) {
            LOGGER.warning("FILE path -> " + path);
            LOGGER.warning("An error has occurred while trying to delete a thumbnail: " + thumbPath);
        }

        File file = new File(path);
        return file.delete();
    }

    private boolean deleteThumbnail(String path) {
        File file = new File(path);
        return file.delete();
    }

    private boolean isValidFileExtension(String fileExtension) {
        String[] validFileExtensions = ConfigurationManager.getValue("private.network.valid.file.extensions").split(",");

        for (String ext : validFileExtensions) {
            if (ext.equals(fileExtension)) {
                return true;
            }
        }
        return false;
    }

}
