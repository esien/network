package novruzov.esien.network.mediamanagement.repository;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.usermanagement.data.User;
import novruzov.esien.network.usermanagement.generic.repository.JpaRepository;
import novruzov.esien.network.usermanagement.service.UserManagementService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Esien Novruzov
 */
@Transactional
public class JpaPictureMetaRepository extends JpaRepository<Picture> implements PictureMetaRepository {

    @Inject
    private UserManagementService userManagementService;

    @Override
    public List<Picture> readAllByUsername(String username) {
        User owner = userManagementService.findUserByUsername(username);
        if (owner == null) {
            return null;
        }

        String query = "from Picture as picture where picture.owner = :owner";
        List<Picture> pictures = entityManager.createQuery(query, Picture.class).setParameter("owner", owner).getResultList();
        return pictures;
    }

    @Override
    public Picture read(String username, String resourceName) {
        User owner = userManagementService.findUserByUsername(username);
        if (owner == null) {
            return null;
        }
        List<Picture> allPictures = readAllByUsername(username);
        for (Picture picture : allPictures){
            if (picture.getResourceName().contains(resourceName)){
                return picture;
            }
        }
        return null;
    }
}
