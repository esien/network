package novruzov.esien.network.mediamanagement.repository;

import javax.servlet.http.Part;

/**
 * @author Esien Novruzov
 */
public interface PictureBlobRepository {

    String persistPicture(Part part);

    String persistPicture(Part part, String path);

    boolean deletePicture(String resourceName);
}
