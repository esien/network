package novruzov.esien.network.mediamanagement.repository;

import novruzov.esien.network.mediamanagement.data.Picture;
import novruzov.esien.network.usermanagement.generic.repository.GenericEntityRepository;

import java.util.List;

/**
 * @author Esien Novruzov
 */
public interface PictureMetaRepository extends GenericEntityRepository<Picture> {

    List<Picture> readAllByUsername(String username);

    Picture read(String username, String fileName);

}
