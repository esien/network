package usermanagement;

import novruzov.esien.network.usermanagement.service.SimpleUserManagementService;
import novruzov.esien.network.usermanagement.service.UserManagementService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Esien Novruzov
 */
@RunWith(JUnit4.class)
public class UserManagementServiceTest {

    private static UserManagementService userManagementService;

    private static final String OLD_PASSWORD = "password1";

    private static final String NEW_PASSWORD = "password2";

    private static final String RETYPED_PASSWORD = "password3";


    @BeforeClass
    public static void setup(){
        userManagementService = new SimpleUserManagementService();
    }



    @Test(expected = IllegalArgumentException.class)
    public void changePasswordTest(){
       userManagementService.changePassword(OLD_PASSWORD, NEW_PASSWORD, RETYPED_PASSWORD);
    }

    @Test
    public void updateInfoTest(){
        assertFalse(userManagementService.updateInfo(null));
    }
}
