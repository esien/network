package uitests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * @author Esien Novruzov
 */
public class SeleniumLoginTest extends AbstractTest {

    private static final String REGISTRATION_URL = "http://localhost:8080/network/registration";

    private static final String USERNAME_ID = "loginForm:username";

    private static final String PASSWORD_ID = "loginForm:inputPassword";

    private static final String SUBMIT_BUTTON_ID = "loginForm:submitButton";

    private static final String PICTURE_DIV_ID = "grid-item";

    private static final int WAIT_TIME = 5000;


    private static final String SIGN_UP_EMAIL = "signupform:email";

    private static final String SIGN_UP_FIRST_NAME = "signupform:firstname";

    private static final String SIGN_UP_LAST_NAME = "signupform:lastname";

    private static final String SIGN_UP_USERNAME = "signupform:username";

    private static final String SIGN_UP_PW = "signupform:password";

    private static final String SIGN_UP_RETYPE_PW = "signupform:retypePassword";

    private static final String SIGN_UP_SUBMIT_BUTTON = "signupform:btn-signup";

    private static int counter;

   @Test
    public void someTest() throws Exception{
       String counter = String.valueOf(new Random().nextInt()).substring(0,4);

       String login_data = counter + "test@gmail.com";
       try {
           driver.get(REGISTRATION_URL);

           registerNewAccount(login_data, login_data, login_data,
                   login_data,login_data, login_data);
           Thread.sleep(WAIT_TIME);

           WebElement username = driver.findElement(By.id(USERNAME_ID));
           WebElement password = driver.findElement(By.id(PASSWORD_ID));
           WebElement submitButton = driver.findElement(By.id(SUBMIT_BUTTON_ID));

           username.sendKeys(login_data);
           password.sendKeys(login_data);
           submitButton.click();

           Thread.sleep(WAIT_TIME);
           List<WebElement> pictureDivs = driver.findElements(By.className(PICTURE_DIV_ID));
           assertTrue("0 picture should be shown, but was " + pictureDivs.size(), pictureDivs.size() == 0);
       } finally {
           driver.quit();
       }
   }

   private void registerNewAccount(String emailValue, String firstNameValue, String lastNameValue,
                                   String usernameValue, String passwordValue, String retypedPasswordValue){
       WebElement email = driver.findElement(By.id(SIGN_UP_EMAIL));
       WebElement firstName = driver.findElement(By.id(SIGN_UP_FIRST_NAME));
       WebElement lastName = driver.findElement(By.id(SIGN_UP_LAST_NAME));
       WebElement userName = driver.findElement(By.id(SIGN_UP_USERNAME));
       WebElement password = driver.findElement(By.id(SIGN_UP_PW));
       WebElement passwordRetyped = driver.findElement(By.id(SIGN_UP_RETYPE_PW));
       WebElement submitButton = driver.findElement(By.id(SIGN_UP_SUBMIT_BUTTON));

       email.sendKeys(emailValue);
       firstName.sendKeys(firstNameValue);
       lastName.sendKeys(lastNameValue);
       userName.sendKeys(usernameValue);
       password.sendKeys(passwordValue);
       passwordRetyped.sendKeys(retypedPasswordValue);
       submitButton.click();
   }
}
