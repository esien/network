package uitests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Esien Novruzov
 */
abstract public class AbstractTest{

    protected WebDriver driver = new ChromeDriver();

}